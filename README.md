# Shared MIME Info

The shared-mime-info package contains:

- The core database of common MIME types, their file extensions and icon names.
- The update-mime-database command, used to extend the DB and install a new MIME data.
- The freedesktop.org shared MIME database spec.

It is used by GLib, GNOME, KDE, XFCE and many others.

For more information about the database see the [Shared MIME Info Specification here](https://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/).

## Build, Test, Package, Install

```sh
# Build
meson --prefix /usr _build
ninja -C _build

# Test
ninja -C _build test

# Packaging
ninja -C _build dist

# Install
ninja -C _build install
```

**Dependencies**

It requires `meson`, `glib` to be installed for building the update command.
Additionally, it uses `gettext` and `itstool` for translations.
See [.gitlab-ci.yml](https://gitlab.freedesktop.org/xdg/shared-mime-info/-/blob/master/.gitlab-ci.yml)
for all dependencies.

**Translation**

This database is translated at Transifex.
See [HACKING.md](https://gitlab.freedesktop.org/xdg/shared-mime-info/-/blob/master/HACKING.md)
for more information about translation.


## Build, Test, Package with Docker

```sh
# Build Docker image
docker build . -t shared-mime-info

# Build, test, package
docker run -it --rm -v $(pwd):/src -w /src shared-mime-info
```

Distribution artefact if available on
`./_build/meson-dist/shared-mime-info-<version>.tar.xz`

## Contributing

- Please report bugs here:
  https://gitlab.freedesktop.org/xdg/shared-mime-info/issues
- Getting involved in freedesktop.org projects guide:
  https://www.freedesktop.org/wiki/GettingInvolved/
- Adding new mime-types:
  https://gitlab.freedesktop.org/xdg/shared-mime-info/-/blob/master/HACKING.md

## Useful reference links

- IANA:
  https://www.iana.org/assignments/media-types/
- KDE's old mime-types:
  http://websvn.kde.org/branches/KDE/3.5/kdelibs/mimetypes/
- UNIX file tool and libmagic
  https://github.com/file/file
