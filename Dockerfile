FROM fedora:rawhide

# Install dependencies
RUN dnf update -y --nogpgcheck \
  && dnf install -y --nogpgcheck gcc gcc-c++ glibc-devel make libxml2-devel \
                                 glib2-devel gettext git itstool xmlto findutils \
                                 gettext-devel meson

# Compile xdgmime
RUN git clone https://gitlab.freedesktop.org/xdg/xdgmime.git \
  && make -C xdgmime

# Compile and test shared-mime-info
RUN echo "meson _build && ninja -C _build && ninja -C _build test dist" > /root/build.sh \
  && chmod +x /root/build.sh

CMD "/root/build.sh"
